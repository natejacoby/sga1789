$(document).ajaxComplete(function () {
    $("#streaming-toggle").click(function () {
        if ($(this).hasClass("hideme")) {
            $(".page-left").css("width", "60%");
            $(".streaming-container").css({
                "transform": "translateX(0)",
                "position": "fixed"
            });
            $(this).removeClass("hideme");
            $(this).css("transform", "rotate(0deg)")
        }
        else {
            $(".page-left").css("width", "100%");
            $(".streaming-container").css({
                "transform": "translateX(100%)"
            });
            $(this).css("transform", "rotate(180deg)");
            $(this).addClass("hideme")
        }
    });
    var text = $(".selection-banner > h1");
    $("#early-shift").mouseover(function () {
        $(text).text("The Early Shift")
    });
    $("#sunrise-show").mouseover(function () {
        $(text).text("Sunrise Show")
    });
    $("#breakfast-show").mouseover(function () {
        $(text).text("The Breakfast Show")
    });
    $("#coffee-club").mouseover(function () {
        $(text).text("Coffee Club")
    });
    $("#midday-music").mouseover(function () {
        $(text).text("Midday Music")
    });
    $("#drive-by-show").mouseover(function () {
        $(text).text("Drive By Show")
    });
    $("#news-hour").mouseover(function () {
        $(text).text("News Hour")
    });
    $("#sports-hour").mouseover(function () {
        $(text).text("Sports Hour")
    });
    $("#wind-down").mouseover(function () {
        $(text).text("The Wind Down")
    });
    //List-View Hover
    var $listItem = $(".list-view > h2");
    $listItem.mouseenter(function () {
        $(this).css("border-left", "4px solid #ff7800");
    });
    $listItem.mouseleave(function () {
        $(this).css("border-left", "none")
    });
    //View Change
    var $list = $(".view-options > img:nth-of-type(1)");
    var $grid = $(".view-options > img:nth-of-type(2)");
    $list.click(function () {
        $(".gallery").hide();
        $(".list-view").show();
        $(this).css("background-color", "#ff7800");
        $grid.css("background-color", "transparent")
    });
    $grid.click(function () {
        $(".gallery").show();
        $(".list-view").hide();
        $(this).css("background-color", "#ff7800");
        $list.css("background-color", "transparent")
    });
    // Clock
    function updateTime() {
        var $time = $('.time');
        var dt = new Date();
        // Fixes the Minutes to be Double Digits
        var time = dt.getHours() + ":" + (dt.getMinutes() < 10 ? '0' : '') + dt.getMinutes();
        var ampm = dt.getHours() >= 12 ? 'pm' : 'am';
        $time.text(time + ampm);
    }

    setInterval(updateTime, 5000);
});