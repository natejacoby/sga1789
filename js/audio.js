/**
 * Created by nate on 3/05/16.
 */
$(document).ready(function () {
    //Change of Icon on Play
    var $playStatus = $("#playStatus");
    var $showSelect = $(".row > div.col-md-4");

    $showSelect.click(function () {
        $playStatus.attr({
            src: 'img/pause.png'
        });
    });

    // Audio Selector
    var audioElement = document.createElement('audio');
    var $nowPlaying = $("#now-playing-title");
    var $trackTitle = $("#track-title");
    var $listItem = $(".list-view > h2");
    var $bannerText = $(".selection-banner > h1");

    $listItem.click(function () {
        $listItem.removeClass("active");
        $(this).toggleClass("active");
        $playStatus.attr({
            src: 'img/pause.png'
        });
    });

    $playStatus.click(function () {

        if ($(this).hasClass('playing')) {
            audioElement.pause();
            $(this).attr({
                src: 'img/play.png',
                class: 'paused'
            })
        }
        else {
            audioElement.play();
            $(this).removeClass('paused').addClass('playing').attr({
                src: 'img/pause.png'
            })
        }
    });

    audioElement.addEventListener("load", function () {
        audioElement.play();
    }, true);

    //AJAX REQUEST URL AND RETURN AS DATA
    $.ajax({
        url: 'information.xml',
        type: 'GET',
        dataType: 'xml',
        success: function (returnedXMLResponse) {
            // ON SUCCESS RUN SUCCESS FUNCTION

            //FOR EACH SHOW
            $('SHOW', returnedXMLResponse).each(function () {
                var show = {};
                //Moving XML into Object for easy access
                $(this).children().each(function () {
                    //Add Field to object for each tag name with the content
                    show[$(this).prop("tagName").toLowerCase()] = $(this).html();
                });
                //Add Generated class value
                show.class = '.' + show.showtitle.replace(/ /g, '-').toLowerCase();
                ;

                $(show.class).click(function () {
                    audioElement.setAttribute('src', show.source);
                    audioElement.play();
                    $nowPlaying.text(show.showtitle);
                    $trackTitle.text(show.track + ' - ' + show.artist);
                });
                $(show.class).hover(function () {
                    $bannerText.text(show.showtitle);
                });
            })
        }
    });


});