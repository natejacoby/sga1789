<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        <?php echo $pageTitle; ?>
    </title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/audio-player.css" rel="stylesheet">
    <link href="css/header.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/selector.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,300,700|Roboto:400,300,500'
          rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"
            integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
    <![endif]-->
</head>
<body>
<div class="page-left">
    <header>
        <h1>The Radio Show Place Thing</h1>
        <img src="img/waveform.png"/>
        <nav>
            <ul>
                <li><a class="<?php if ($section == "home") {
                        echo "on";
                    } ?>" href="index.php?">Home</a></li>
                <li><a class="<?php if ($section == "about") {
                        echo "on";
                    } ?>" href="about.php?">About</a></li>
                <li><a class="<?php if ($section == "shows") {
                        echo "on";
                    } ?>" href="#">Shows</a></li>
                <li><a class="<?php if ($section == "pictures") {
                        echo "on";
                    } ?>" href="#">Pictures</a></li>
                <li><a class="<?php if ($section == "contact") {
                        echo "on";
                    } ?>" href="#">Contact</a></li>
            </ul>
        </nav>
    </header>
    <div class="streaming-container">
    </div>
    <div id="ajax-toggle">
        <img src="img/music.png"/>
        <h1>Listen</h1>
    </div>
    <section class="main-content-container container-fluid">