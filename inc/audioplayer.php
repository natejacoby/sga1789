<img src="img/arrow.png" class="streaming-toggle" id="streaming-toggle"/>
<div class="streaming-title">
    <img src="img/music.png" id="toggle-arrow"/>
    <h1>
        On Demand
    </h1>
</div>
<div>
    <p class="time">12:05</p>
</div>
<div class="view-options">
    <img src="img/list.png"/>
    <img src="img/grid.png"/>
</div>
<div class="container-fluid gallery">
    <div class="row gap">
        <div class="col-md-4">
            <img src="img/01.jpg" class="img-responsive the-early-shift"/>
        </div>
        <div class="col-md-4">
            <img src="img/02.jpg" class="img-responsive sunrise-show"/>
        </div>
        <div class="col-md-4">
            <img src="img/03.jpg" class="img-responsive breakfast-show"/>
        </div>
    </div>
    <div class="row gap">
        <div class="col-md-4">
            <img src="img/04.jpg" class="img-responsive coffee-club"/>
        </div>
        <div class="col-md-4">
            <img src="img/05.jpg" class="img-responsive midday-music"/>
        </div>
        <div class="col-md-4">
            <img src="img/06.jpg" class="img-responsive drive-by-show"/>
        </div>
    </div>
    <div class="row gap">
        <div class="col-md-4">
            <img src="img/07.jpg" class="img-responsive news-hour"/>
        </div>
        <div class="col-md-4">
            <img src="img/08.jpg" class="img-responsive sports-hour"/>
        </div>
        <div class="col-md-4">
            <img src="img/09.jpg" class="img-responsive the-wind-down"/>
        </div>
    </div>
    <div class="selection-container">
        <div class="selection-banner">
            <h1>
                The Early Show
            </h1>
        </div>
    </div>
</div>
<div class="list-view">

    <h2 class="early-shift" value="The Early Shift">The Early Shift</h2>
    <h2 class="sunrise-show">Sunrise Show</h2>
    <h2 class="breakfast-show">The Breakfast Show</h2>
    <h2 class="coffee-club">Coffee Club</h2>
    <h2 class="midday-music">Midday Music</h2>
    <h2 class="drive-by-show">Drive By Show</h2>
    <h2 class="news-hour">News Hour</h2>
    <h2 class="sports-hour">Sports Hour</h2>
    <h2 class="wind-down">The Wind Down</h2>
    <h2 class="night-sounds">Night Sounds</h2>
</div>
<?php include('show.php') ?>

<div class="audio-player-container">
    <img src="<?php echo $image; ?>" class="album-cover"/>
    <div class="now-playing-title">
        <p id="track-title">
            <?php echo $segment_title . ' | '; ?>
        </p>
        <p class="now">
            <?php echo $song_title . ' - ' . $song_artist; ?>
        </p>
    </div>
    <div class="audio-controller">
        <img src="img/rewind.png"/>
        <img src="img/play.png" id="playStatus"/>
        <img src="img/forward.png"/>
    </div>
</div>
<script src="js/audio.js" type="application/javascript"></script>
