<?php
$servername = "localhost";
$username = "root";
$password = "";
$db = 'radio';

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = 'SELECT * FROM Shows WHERE ' . date("H") . 'BETWEEN startdate-1 AND enddate+1';

if ($result = $conn->query($sql)) {
    while ($obj = $result->fetch_object()) {
        $image = $obj->image;
        $media = $obj->media;
        $segment_title = $obj->segment_title;
        $song_title = $obj->song_title;
        $song_artist = $obj->song_artist;
    }
    $result->close();
} else {
    echo mysqli_error($conn);
}

$conn->close();