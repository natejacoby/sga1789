-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 23, 2016 at 02:27 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `radio`
--

-- --------------------------------------------------------

--
-- Table structure for table `Shows`
--

CREATE TABLE `Shows` (
  `ID` int(2) NOT NULL,
  `start_time` int(2) NOT NULL,
  `end_time` int(2) NOT NULL,
  `segment_title` varchar(64) NOT NULL,
  `image` varchar(64) NOT NULL,
  `media` varchar(64) NOT NULL,
  `song_title` varchar(64) NOT NULL,
  `song_artist` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Shows`
--

INSERT INTO `Shows` (`ID`, `start_time`, `end_time`, `segment_title`, `image`, `media`, `song_title`, `song_artist`) VALUES
(1, 1, 4, 'The Early Shift', 'img/01.jpg', 'music/01.wav', 'Color Of My Soul', 'Pretty Lights'),
(2, 5, 6, 'Sunrise Show', 'img/02.jpg', 'music/02.wav', 'Press Pause', 'Pretty Lights'),
(3, 7, 9, 'Breakfast Show', 'img/03.jpg', 'music/03.wav', 'Let''s Get Busy', 'Pretty Lights'),
(4, 10, 11, 'Coffee Club', 'img/04.jpg', 'music/04.wav', 'Around The Block feat Talib Kweli', 'Pretty Lights'),
(5, 12, 14, 'Midday Music', 'img/05.jpg', 'music/05.wav', 'Yellow Bird', 'Pretty Lights'),
(6, 15, 17, 'Drive By Show', 'img/06.jpg', 'music/06.wav', 'Go Down Sunshine', 'Pretty Lights'),
(7, 18, 18, 'News Hour', 'img/07.jpg', 'music/07.wav', 'So Bright feat. Eligh', 'Pretty Lights'),
(8, 19, 19, 'Sports Hour', 'img/08.jpg', 'music/08.wav', 'Vibe Vendetta', 'Pretty Lights'),
(9, 20, 22, 'The Wind Down', 'img/09.jpg', 'music/09.wav', 'Done Wrong', 'Pretty Lights'),
(10, 23, '0', 'Night Sounds', 'img/10.jpg', 'music/10.wav', 'Prophet', 'Pretty Lights');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Shows`
--
ALTER TABLE `Shows`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Shows`
--
ALTER TABLE `Shows`
  MODIFY `ID` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
